﻿using System;
using m1.Models;
using Microsoft.AspNetCore.Mvc;

namespace m1.Controllers {
    public class CalcServiceController : Controller {
        private readonly Random rand;

        private readonly int x;
        private readonly int y;

        private readonly string sum;
        private readonly string sub;
        private readonly string mp;
        private readonly string div;

        public CalcServiceController() {
            rand = new Random();

            x = rand.Next() % 11;
            y = rand.Next() % 11;

            sum = $"{x} + {y} = {x + y}";
            sub = $"{x} - {y} = {x - y}";
            mp = $"{x} * {y} = {x * y}";

            try
            {
                div = $"{x} /  {y} = {x / y}";
            }
            catch (DivideByZeroException)
            {
                div = "zero division error";
            }
        }

        public IActionResult Index() {
            return View();
        }

        public IActionResult ByModel() {
            var model = new CalcModel(x, y, sum, sub, mp, div);
            return View(model);
        }

        public IActionResult ByViewData() {
            ViewData["X"] = x;
            ViewData["Y"] = y;

            ViewData["Sum"] = sum;
            ViewData["Sub"] = sub;
            ViewData["Mp"] = mp;
            ViewData["Div"] = div;

            return View();
        }

        public IActionResult ByViewBag() {
            ViewBag.X = x;
            ViewBag.Y = y;

            ViewBag.Sum = sum;
            ViewBag.Sub = sub;
            ViewBag.Mp = mp;
            ViewBag.Div = div;

            return View();
        }

        public IActionResult ByDirect() {
            var model = new CalcModel(x, y, sum, sub, mp, div);
            return View(model);
        }
    }
}
